# Godot Engine Scene Instancing Test - Instances a scene from GDScript.
#
# Written in 2019 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node

func _ready():
	var scene = preload("res://GodotGuy.tscn")
	var node = scene.instance()
	add_child(node)
