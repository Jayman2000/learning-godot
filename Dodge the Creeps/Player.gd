# Created using the "Your first game" tutorial by Juan Linietsky, Ariel Manzur
# and the Godot community
#
# <https://docs.godotengine.org/en/3.2/getting_started/step_by_step/your_first_game.html>

extends Area2D

signal hit

export var speed = 400 # How fast the player will move (pixels/second)
var screen_size
var target = Vector2()

func _ready():
	hide()
	screen_size = get_viewport_rect().size

func _process(delta):
	var velocity = Vector2()

	if position.distance_to(target) > 10:
		velocity = target - position

#	if Input.is_action_pressed("ui_left"):
#		velocity.x -= 1
#	if Input.is_action_pressed("ui_right"):
#		velocity.x += 1
#	if Input.is_action_pressed("ui_up"):
#		velocity.y -= 1
#	if Input.is_action_pressed("ui_down"):
#		velocity.y += 1

	if velocity.length() > 0:
		# Player is trying to move.
		velocity = velocity.normalized() * speed * delta
		position += velocity
		position.x = clamp(position.x, 0, screen_size.x)
		position.y = clamp(position.y, 0, screen_size.y)
		$AnimatedSprite.play()
	else:
		# Player is not trying to move
		$AnimatedSprite.stop()

	if velocity.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y != 0:
		$AnimatedSprite.animation = "up"
		$AnimatedSprite.flip_v = velocity.y > 0

func _input(event):
	if event is InputEventScreenTouch and event.pressed:
		target = event.position

func _on_Player_body_entered(_body):
	hide()
	emit_signal("hit")
	$CollisionShape2D.set_deferred("disabled", true)

func start(pos):
	position = pos
	target = pos
	show()
	$CollisionShape2D.disabled = false
