# Created using the "Your first game" tutorial by Juan Linietsky, Ariel Manzur
# and the Godot community
#
# <https://docs.godotengine.org/en/3.2/getting_started/step_by_step/your_first_game.html>

extends RigidBody2D

export var min_speed = 150
export var max_speed = 250

func _ready():
	var mob_types = $AnimatedSprite.frames.get_animation_names()
	$AnimatedSprite.animation = mob_types[randi() % mob_types.size()]

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
