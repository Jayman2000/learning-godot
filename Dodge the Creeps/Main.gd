# Created using the "Your first game" tutorial by Juan Linietsky, Ariel Manzur
# and the Godot community
#
# <https://docs.godotengine.org/en/3.2/getting_started/step_by_step/your_first_game.html>

extends Node

export (PackedScene) var Mob
var score

func _ready():
	randomize()

func new_game():
	$Music.play()

	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()

	$HUD.update_score(score)
	$HUD.show_message("Get Ready")

func game_over():
	$DeathSound.play()
	$Music.stop()

	$ScoreTimer.stop()
	$MobTimer.stop()
	get_tree().call_group("mobs", "queue_free")

	$HUD.show_game_over()

func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()

func _on_ScoreTimer_timeout():
	score += 1

	$HUD.update_score(score)

func _on_MobTimer_timeout():
	# Choose a random location along the Path2D
	$MobPath/MobSpawnLocation.offset = randi()
	# Create a mob instance and add it to the scene
	var mob = Mob.instance()
	add_child(mob)
	# Set the mob's direction perpendicular to the path direction
	var direction = $MobPath/MobSpawnLocation.rotation + TAU / 4
	# Set the mob's position to a random location
	mob.position = $MobPath/MobSpawnLocation.position
	# Add some randomness to the direction
	direction += rand_range(-TAU / 8, TAU / 8)
	mob.rotation = direction
	# Set the velocity
	mob.linear_velocity = Vector2(rand_range(mob.min_speed, mob.max_speed), 0)
	mob.linear_velocity = mob.linear_velocity.rotated(direction)
