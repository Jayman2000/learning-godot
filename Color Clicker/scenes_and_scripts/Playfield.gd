# Color Clicker - A game about clicking on dots.
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node2D

# The total number of dots on the screen.
const TOTAL = 30
const DOT = preload("res://scenes_and_scripts/WhatYouClickOn.tscn")
const GAME_OVER_SCREEN = preload("res://scenes_and_scripts/GameOverScreen.tscn")

# Black, White, Red, Green and Blue
const POSSIBLE_COLORS = [Color(0, 0, 0, 1), Color(1, 1, 1, 1),
						 Color(1, 0, 0, 1), Color(0, 1, 0, 1),
						 Color(0, 0, 1, 1)]

# The area in which dots can be placed.
onready var area = get_viewport_rect().size
onready var next_colors = []
onready var score = 0

var bonus_color

func _ready():
	randomize()
	change_bonus_color()
	for _i in range(TOTAL):
		spawn_dot()

func _on_dot_clicked(color):
	if color == bonus_color:
		score += 10
	else:
		score += 1
	$ScorePanel/ScoreLabel.text = "Score: " + str(score)

	spawn_dot()

# Returns the color that the next dot should be.
func get_next_color():
	if next_colors.empty():
		next_colors = POSSIBLE_COLORS.duplicate()
		next_colors.shuffle()
	return next_colors.pop_front()

func change_bonus_color():
	bonus_color = POSSIBLE_COLORS[randi() % POSSIBLE_COLORS.size()]
	$ScorePanel/BonusSprite.modulate = bonus_color

func spawn_dot():
	var to_spawn = DOT.instance()

	to_spawn.connect("clicked", self, "_on_dot_clicked")

	to_spawn.get_node("Sprite").modulate = get_next_color()

	var x = rand_range(0, area.x)
	var y = rand_range(9, area.y)
	to_spawn.position = Vector2(x, y)

	add_child(to_spawn)

func game_over():
	for child in get_children():
		child.queue_free()

	var to_add = GAME_OVER_SCREEN.instance()
	to_add.get_node("Score").text = str(score)
	add_child(to_add)
