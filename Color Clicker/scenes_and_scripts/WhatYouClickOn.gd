# Color Clicker - A game about clicking on dots.
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Area2D

signal clicked(color)

func _input_event(_viewport, event, _shape_idx):
	if event.is_pressed():
		emit_signal("clicked", $Sprite.modulate)
		queue_free()
