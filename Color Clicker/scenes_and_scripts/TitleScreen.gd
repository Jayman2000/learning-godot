# Color Clicker - A game about clicking on dots.
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Panel

func _on_Button_pressed():
	var result = get_tree().change_scene("res://scenes_and_scripts/Playfield.tscn")
	if result != OK:
		print("ERROR: couldn't load the playfield")
		get_tree().quit(70)
