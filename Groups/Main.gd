# Groups - Test of Godot Engine's group functionality
#
# Written in 2019 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Control

func _ready():
	get_node("Alarm/Button").connect("pressed", self, "_on_Button_pressed")

func _on_Button_pressed():
	get_tree().call_group("enemies", "alert")
	print("Here's a list of enemies that were alerted:")
	for enemy in get_tree().get_nodes_in_group("enemies"):
		print("    "  + enemy.name)
