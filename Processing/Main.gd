extends Label

var acum = 0

func _process(delta):
	acum += delta
	text = str(acum)
