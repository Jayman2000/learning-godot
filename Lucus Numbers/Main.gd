# Godot Engine Lucas Number Generator - Allows the user to calculate arbitrarily long sequences of lucas numbers.
#
# Written in 2019 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Panel

var first2 = []
var last2 = []

func _ready():
	get_node("HBoxContainer/Button").connect("pressed", self, "_on_Button_pressed")


func _on_Button_pressed():
	if get_node("HBoxContainer/VBoxContainer/FirstNumber/Input").text.is_valid_float() and \
	   get_node("HBoxContainer/VBoxContainer/SecondNumber/Input").text.is_valid_float():
		if first2.empty():
			reset()
			return
		var a = float(get_node("HBoxContainer/VBoxContainer/FirstNumber/Input").text)
		var b = float(get_node("HBoxContainer/VBoxContainer/SecondNumber/Input").text)
		if first2[0] != a or first2[1] != b:
			reset()
		else:
			var c = last2[0] + last2[1]
			last2[0] = last2[1]
			last2[1] = c
			get_node("Sequence").text += ", " + str(c)
			update_golden_ratio()
	else:
		get_node("Sequence").text = "ERROR!"
		get_node("GoldenRatio").text = ""

# Only call reset if you know that the text boxes contain valid integers
func reset():
	var first_number = get_node("HBoxContainer/VBoxContainer/FirstNumber/Input").text
	var second_number = get_node("HBoxContainer/VBoxContainer/SecondNumber/Input").text
	get_node("Sequence").text = first_number + ", " + second_number
	first2 = [float(first_number), float(second_number)]
	last2 = [float(first_number), float(second_number)]
	update_golden_ratio()

# last2 must contain 2 floats before update_golden_ratio is called
func update_golden_ratio():
	if last2[0] != 0:
		var phi = last2[1] / last2[0]
		get_node("GoldenRatio").text = "The golden ratio is approximately " + str(phi)
	else:
		get_node("GoldenRatio").text = ""
