extends Sprite

func blink():
	visible = !visible

func _on_CustomSignals_a_new_custom_signal():
	$Timer.connect("timeout", self, "blink")
	$Timer.start()

func change_modulation(color):
	modulate = color
