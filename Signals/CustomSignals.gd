extends Node2D

signal a_new_custom_signal
signal change_modulation(color)

func _ready():
	connect("change_modulation", $BlinkingSprite, "change_modulation")

	emit_signal("a_new_custom_signal")
	emit_signal("change_modulation", Color(1, 0, 0, 0.5))
