This repository keeps track of my progress through the Godot Documentation's tutorials.
#### Copying
Unless otherwise noted, the files and folders in this repository as well as their contents have been dedicated to the public domain using [CC0](https://creativecommons.org/publicdomain/zero/1.0/) (see COPYING.txt).
