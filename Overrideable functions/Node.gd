# Godot Engine Overrideable Functions Test - Let's see what happens in what order.
#
# Written in 2019 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node

var process_count = 0
var physics_process_count = 0

func named_print(var message):
	print(name + ": " + message)

func _enter_tree():
	named_print("_enter_tree")

func _ready():
	named_print("_ready")

func _exit_tree():
	named_print("_exit_tree")

func _process(delta):
	named_print("_process")
	process_count += 1
	if process_count >= 10:
		set_process(false)

func _physics_process(delta):
	named_print("_physics_process")
	physics_process_count += 1
	if physics_process_count >= 10:
		set_physics_process(false)
