# Godot Engine Notifications Test - Test of Godot Engine's notification functionality
#
# Written in 2019 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Label

var frame_count

func _notification(what):
	match what:
		NOTIFICATION_READY:
			frame_count = 0
			# This is important. If a script does not have an _process function, then it doesn't process by default.
			set_process(true)
		NOTIFICATION_PROCESS:
			frame_count += 1
			text = str(frame_count)
